import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "It works" example
    router.get { req in
        return "It works!"
    }
    
    // Basic "Hello, world!" example
    router.get("hello") { req in
        return "Hello, world!"
    }

    router.get("upgrade-information") {  req in
        return "https://www.shepherdapp.io/shepherd-pro"
    }
    
    router.get("help") {  req in
        return "https://www.shepherdapp.io/how-to-use"
    }
}
